///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.umsa.wsDocente.util;
//
//import java.util.HashMap;
//import java.util.List;
//
//import org.apache.http.HttpResponse;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.client.methods.HttpPut;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.protocol.HTTP;
//import org.apache.http.util.EntityUtils;
//import org.codehaus.jackson.map.ObjectMapper;
//import org.codehaus.jackson.type.TypeReference;
//import org.springframework.stereotype.Component;
//
///**
// *
// * @author ocontreras
// */
//
//@Component
//public class RestUtil {
//	private HttpClient client;
//	
//	public RestUtil()
//	{
//		
//	}
//	
//	public RestUtil(String user, String password, String authUrl)
//	{
//		if (!authenticate(authUrl, user, password))
//		{
//			throw new IllegalArgumentException("Cannot authenticate user " + user + " on " + authUrl);
//		}
//	}
//	
//	public boolean authenticate(String authUrl, String user, String password)
//	{
//		boolean returnedValue = false;
//		
//		try
//		{
//			HttpPost httpPost = new HttpPost(authUrl);
//			String jsonObj = "{\"username\": \"" + user + "\", \"password\": \"" + password + "\"}";
//			StringEntity entity = new StringEntity(jsonObj.toString(), HTTP.UTF_8);
//			entity.setContentType("application/json");
//			httpPost.setEntity(entity);
//			client = new DefaultHttpClient();
//			HttpResponse response = client.execute(httpPost);
//			
//			HashMap<String, Object> result = new ObjectMapper().readValue(EntityUtils.toString(response.getEntity()), new TypeReference<HashMap<String, Object>>() {});
//			
//			returnedValue = (Boolean)result.get("authenticationSuccess");
//			
//			if (!returnedValue)
//			{
//				client = null;
//			}
//			
//		}
//		catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//		return returnedValue;
//	}
//	
//	public HttpResponse callJSONService(String url, HashMap params, String method)
//	{
//		HashMap returnedObject = null;
//		String jsonObj = null;
//		ObjectMapper mapper = new ObjectMapper();
//		HttpGet get = null;
//		HttpPost post = null;
//		HttpPut put = null;
//		StringEntity stringEntity = null;
//		HttpResponse response = null;
//		
//		if (client == null)
//		{
//			throw new IllegalArgumentException("No HTTP client opened.");
//		}
//		
//		try
//		{
//			if (params != null)
//			{
//				jsonObj = mapper.writeValueAsString(params);
//				stringEntity = new StringEntity(jsonObj.toString(), HTTP.UTF_8);
//				stringEntity.setContentType("application/json");
//			}
//			
//			if ("GET".equals(method))
//			{
//				get = new HttpGet(url);
//				response = client.execute(get);
//			}
//			else if ("POST".equals(method))
//			{
//				post = new HttpPost(url);
//				
//				if (stringEntity != null)
//				{
//					post.setEntity(stringEntity);
//				}
//				
//				response = client.execute(post);
//			}
//			else if ("PUT".equals(method))
//			{
//				put = new HttpPut(url);
//				
//				if (stringEntity != null)
//				{
//					put.setEntity(stringEntity);
//				}
//				
//				response = client.execute(put);
//			}
//			
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
//		
//		return response;
//	}
//	
//	public HashMap callJSONServiceForResult(String url, HashMap params, String method)
//	{
//		HashMap map = null; 
//		
//		try
//		{
//			map = new ObjectMapper().readValue(EntityUtils.toString(callJSONService(url, params, method).getEntity()), new TypeReference<HashMap>() {});
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
//		
//		return map;
//	}
//	
//	public List<HashMap> callJSONServiceForList(String url, HashMap params, String method)
//	{
//		List<HashMap> map = null; 
//		
//		try
//		{
//			map = new ObjectMapper().readValue(EntityUtils.toString(callJSONService(url, params, method).getEntity()), new TypeReference<List<HashMap>>() {});
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
//		
//		return map;
//	}
//
//}
