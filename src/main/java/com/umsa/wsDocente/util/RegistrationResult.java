package com.umsa.wsDocente.util;

public class RegistrationResult {
	private String message;
	private Integer code;

	public RegistrationResult() {
		this.message = "Registro Exitoso";
		this.code = 1;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
