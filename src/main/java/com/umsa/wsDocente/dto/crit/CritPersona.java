package com.umsa.wsDocente.dto.crit;

import java.util.Date;

public class CritPersona {
	private Long idPersona;
	private Integer idSexo;
	private String apPaterno;
	private String apMaterno;
	private String apCasada;
	private String nombres;
	private String nroCi;
	private String extCi;
	private String lugCi;
	private Date fecNacimiento;
	private String dirFoto;
	private String direccion;
	private String telefonos;
	private String correos;
	private String observacion;
	private Integer estado;
	private Date fecReg;
	private Long usuReg;
	private Date fecMod;
	private Long usuMod;
	
	public CritPersona() {
		super();
	}

	@Override
	public String toString() {
		return "CritPersona [idPersona=" + idPersona + ", idSexo=" + idSexo + ", apPaterno=" + apPaterno
				+ ", apMaterno=" + apMaterno + ", apCasada=" + apCasada + ", nombres=" + nombres + ", nroCi=" + nroCi
				+ ", extCi=" + extCi + ", lugCi=" + lugCi + ", fecNacimiento=" + fecNacimiento + ", dirFoto=" + dirFoto
				+ ", direccion=" + direccion + ", telefonos=" + telefonos + ", correos=" + correos + ", observacion="
				+ observacion + ", estado=" + estado + ", fecReg=" + fecReg + ", usuReg=" + usuReg + ", fecMod="
				+ fecMod + ", usuMod=" + usuMod + "]";
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Integer getIdSexo() {
		return idSexo;
	}

	public void setIdSexo(Integer idSexo) {
		this.idSexo = idSexo;
	}

	public String getApPaterno() {
		return apPaterno;
	}

	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}

	public String getApMaterno() {
		return apMaterno;
	}

	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}

	public String getApCasada() {
		return apCasada;
	}

	public void setApCasada(String apCasada) {
		this.apCasada = apCasada;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getNroCi() {
		return nroCi;
	}

	public void setNroCi(String nroCi) {
		this.nroCi = nroCi;
	}

	public String getExtCi() {
		return extCi;
	}

	public void setExtCi(String extCi) {
		this.extCi = extCi;
	}

	public String getLugCi() {
		return lugCi;
	}

	public void setLugCi(String lugCi) {
		this.lugCi = lugCi;
	}

	public Date getFecNacimiento() {
		return fecNacimiento;
	}

	public void setFecNacimiento(Date fecNacimiento) {
		this.fecNacimiento = fecNacimiento;
	}

	public String getDirFoto() {
		return dirFoto;
	}

	public void setDirFoto(String dirFoto) {
		this.dirFoto = dirFoto;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(String telefonos) {
		this.telefonos = telefonos;
	}

	public String getCorreos() {
		return correos;
	}

	public void setCorreos(String correos) {
		this.correos = correos;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Date getFecReg() {
		return fecReg;
	}

	public void setFecReg(Date fecReg) {
		this.fecReg = fecReg;
	}

	public Long getUsuReg() {
		return usuReg;
	}

	public void setUsuReg(Long usuReg) {
		this.usuReg = usuReg;
	}

	public Date getFecMod() {
		return fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Long getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(Long usuMod) {
		this.usuMod = usuMod;
	}

	public CritPersona(Long idPersona, Integer estado) {
		super();
		this.idPersona = idPersona;
		this.estado = estado;
	}
	
}
