package com.umsa.wsDocente.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.umsa.wsDocente.dto.Docente;
import com.umsa.wsDocente.dto.crit.CritDocente;
import com.umsa.wsDocente.service.DocenteService;
import com.umsa.wsDocente.service.PersonaService;
import com.umsa.wsDocente.util.RegistrationResult;

@RestController
//@PreAuthorize("hasRole('ROLE_AUTORIDAD_1')")
//@PreAuthorize("hasRole('ROLE_DOCENTE')")
@CrossOrigin(origins = "http://localhostt")
public class DocenteController {
	@Autowired
	PersonaService personaService;
	@Autowired
	DocenteService docenteService;
	@Autowired
	private ResourceLoader resourceLoader;
	
	
	@PostMapping("api/autoridad/docentes/listarDocentes")
	public List<Docente> listarDocentes(@RequestBody CritDocente critDocente) {
		critDocente.setEstado(1);
		return docenteService.listarDocente(critDocente);
	}
	
	@GetMapping("api/autoridad/docentes/obtenerDocente/{idDocente}")
	public Docente obtenerDcoente(@PathVariable Long idDocente) {
		CritDocente critDocente = new CritDocente(idDocente, 1);
		List<Docente> listaDocente = docenteService.listarDocente(critDocente);
		if (listaDocente.size() == 1) {
			return listaDocente.get(0);
		} else {
			return new Docente();
		}
	}

	@PostMapping("api/autoridad/docentes/registrarDocente")
	public RegistrationResult registrarDocente(@RequestBody Docente docente) {
		docente.setEstado(1);
		return docenteService.registrarDocente(docente);
	}

	@PostMapping("api/autoridad/docentes/eliminarDocente")
	public RegistrationResult eliminarDocente(@RequestBody Docente docente) {
		return docenteService.eliminarDocente(docente);
	}
}