package com.umsa.wsDocente;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.umsa.wsDocente.dao")
public class WsDocenteApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsDocenteApplication.class, args);
	}

}
