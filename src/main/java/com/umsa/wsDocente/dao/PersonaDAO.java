package com.umsa.wsDocente.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.umsa.wsDocente.dto.Persona;
import com.umsa.wsDocente.dto.crit.CritPersona;

@Repository
public interface PersonaDAO {
	List <Persona> listarPersona(CritPersona Persona);
	Integer actualizarPersona(Persona persona);
	Integer registrarPersona(Persona persona);
	Integer eliminarPersona(Persona persona);
}
