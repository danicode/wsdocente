package com.umsa.wsDocente.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.umsa.wsDocente.dto.Docente;
import com.umsa.wsDocente.dto.crit.CritDocente;

@Repository
public interface DocenteDAO {
	List <Docente> listarDocente(CritDocente critDocente);
	Integer registrarDocente(Docente docente);
	Integer actualizarDocente(Docente docente);
	Integer eliminarDocente(Docente docente);
}
