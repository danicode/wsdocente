package com.umsa.wsDocente.service;

import java.util.List;

import com.umsa.wsDocente.dto.Persona;
import com.umsa.wsDocente.dto.crit.CritPersona;
import com.umsa.wsDocente.util.RegistrationResult;

public interface PersonaService {
	List <Persona> listarPersona(CritPersona critPersona);
	RegistrationResult registrarPersona(Persona persona);
	RegistrationResult eliminarPersona(Persona persona);
}
	
