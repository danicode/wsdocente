package com.umsa.wsDocente.service;

import java.util.List;

import com.umsa.wsDocente.dto.Docente;
import com.umsa.wsDocente.dto.crit.CritDocente;
import com.umsa.wsDocente.util.RegistrationResult;

public interface DocenteService {
	List<Docente> listarDocente(CritDocente critDocente);
	RegistrationResult registrarDocente (Docente estudiante);
	RegistrationResult eliminarDocente (Docente estudiante);
}
