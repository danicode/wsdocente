package com.umsa.wsDocente.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.umsa.wsDocente.dao.PersonaDAO;
import com.umsa.wsDocente.dto.Persona;
import com.umsa.wsDocente.dto.crit.CritPersona;
import com.umsa.wsDocente.service.PersonaService;
import com.umsa.wsDocente.util.Constantes;
import com.umsa.wsDocente.util.RegistrationResult;

@Service
@Transactional
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	private PersonaDAO personaDAO;

	@Override
	public List<Persona> listarPersona(CritPersona critPersona) {

		return personaDAO.listarPersona(critPersona);
	}

	@Override
	public RegistrationResult registrarPersona(Persona persona) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			if (persona.getIdPersona() == null) {
				personaDAO.registrarPersona(persona);
			} else {
				personaDAO.actualizarPersona(persona);
			}
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.REGISTRO_FALLIDO + e.getMessage());
		}
		return registrationResult;
	}

	@Override
	public RegistrationResult eliminarPersona(Persona persona) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			personaDAO.eliminarPersona(persona);
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.ELIMINACION_FALLIDA);
		}
		return registrationResult;
	}

}
