package com.umsa.wsDocente.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.umsa.wsDocente.dao.DocenteDAO;
import com.umsa.wsDocente.dto.Docente;
import com.umsa.wsDocente.dto.crit.CritDocente;
import com.umsa.wsDocente.service.DocenteService;
import com.umsa.wsDocente.util.Constantes;
import com.umsa.wsDocente.util.RegistrationResult;

@Service
@Transactional
public class DocenteServiceImpl implements DocenteService {

	@Autowired
	private DocenteDAO docenteDAO;
	

	@Override
	public List<Docente> listarDocente(CritDocente critDocente) {
		return docenteDAO.listarDocente(critDocente);
	}

	@Override
	public RegistrationResult registrarDocente(Docente docente) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			if (docente.getIdDocente() == null) {
				docenteDAO.registrarDocente(docente);
			} else {
				docenteDAO.actualizarDocente(docente);
			}
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.REGISTRO_FALLIDO + e.getMessage());
		}
		return registrationResult;
	}

	@Override
	public RegistrationResult eliminarDocente(Docente docente) {
		RegistrationResult registrationResult = new RegistrationResult();
		try {
			docenteDAO.eliminarDocente(docente);
		} catch (Exception e) {
			registrationResult.setCode(0);
			registrationResult.setMessage(Constantes.ELIMINACION_FALLIDA);
		}
		return registrationResult;
	}


}
