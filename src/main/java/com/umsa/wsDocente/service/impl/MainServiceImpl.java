//package com.umsa.wsDocente.service.impl;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.umsa.wsDocente.dao.AdmUsuarioDAO;
//import com.umsa.wsDocente.dto.AdmUsuario;
//import com.umsa.wsDocente.dto.crit.CritAdmUsuario;
//import com.umsa.wsDocente.service.MainService;
//
//@Service
//@Transactional
//public class MainServiceImpl implements MainService {
//
//	@Autowired
//	private AdmUsuarioDAO admUsuarioDAO;
//
//	@Override
//	public List<AdmUsuario> listarUsuario(CritAdmUsuario critAdmUsuario) {
//		return admUsuarioDAO.listarUsuarios(critAdmUsuario);
//	}
//	
//	
//}
